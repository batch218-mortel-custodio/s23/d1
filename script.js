////// OBJECTS!!!!!!


/*
    - An object is a data type that is used to represent real world objects
    - It is a collection of related data and/or functionalities
    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
    - Information stored in objects are represented in a "key:value" pair
    - A "key" is also mostly referred to as a "property" of an object
    - Different data types may be stored in an object's property creating complex data structures
*/


// Creating objects using object initializers/literal notation

/*
    - This creates/declares an object and also initializes/assigns it's properties upon creation
    - A cellphone is an example of a real world object
    - It has it's own properties such as name, color, weight, unit model and a lot of other things
    - Syntax
        let objectName = {
            keyA: valueA,
            keyB: valueB
        }
*/


let cellphone = {
    name: 'Nokia 3210',
    manufactureDate: 1999
};

console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a constructor function
/*
    - Creates a reusable function to create several objects that have the same data structure
    - This is useful for creating multiple instances/copies of an object
    - An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
    - Syntax
        function ObjectName(keyA, keyB) {
            this.keyA = keyA;
            this.keyB = keyB;
        }
*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
function Laptop(name, manufactureDate){
    this.name = name;
    this.manufactureDate = manufactureDate;
};

let laptop = new Laptop("HP", 1925);
console.log(laptop);

let myLaptop = new Laptop("Dell", 1876);
console.log(myLaptop);

let oldLaptop = new Laptop('Portal R2E CCMC', 1980); // need 'new' keyword
console.log('Result from creating objects without the new keyword:');
console.log(oldLaptop);




//creating empty objects
let computer = {};
let myComputer = new Object();


/*let list = laptop.concat(myLaptop);
console.log(list);*/


// accessing object properties

// using dot notation            object name / property
console.log("Result from dot notation: "+ myLaptop.name);

// using square bracket

console.log("Result from dot notation: "+ myLaptop['name']);




let array =  [laptop,  myLaptop, oldLaptop];
console.log(array);

console.log(array[0]['name']);
console.log(array[0].name);


// [Section] Initializing/Adding/Deleting/Reassigning Object Properties

/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/

console.log("----------------//---------------------");

let car = {};  //initialization

// 
car.name = "Honda Civic";
car.color = "Red"
console.log(car);

car['manufacture date'] = 2019; // use this if may space yung attri
console.log(car['manufacture date']);
console.log(car);

delete car['manufacture date'];
console.log(car);

// Reassigning object properties

car.name = "Dodge Charger R/T";
console.log(car);



// OBJECT METHODS

/*
    - A method is a function which is a property of an object
    - They are also functions and one of the key differences they have is that methods are functions related to a specific object
    - Methods are useful for creating object specific functions which are used to perform tasks on them
    - Similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how it should work
*/

// FUNCTION INSIDE as an  OBJECT property

let person =  {
    name: 'John',
    talk: function(){
        console.log("HELLOW MA NEM IS "+ this.name); // this.name is the same with person.name
    // we use 'this' keyword to access a property/attributw inside
    }
}

console.log(person);
console.log("Result from object methods: ");
person.talk();


// adding methods to objects
person.walk = function(){
    console.log(this.name+ ' walked 25 steps forward.')
}
person.walk();


person.walk = function(){
    console.log(this.name+ ' walked 30 steps forward.')
}
person.walk();



console.log("----------------//---------------------");

// methods are useful for creating reusable functions that perform ttasks related to objects


function Pokemon(name, level){


    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;
/*    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,*/
    this.tackle = function(target){
        console.log(this.name +" tackled" + target.name);
        target.health -= this.attack;
        console.log(target.name + " health is now reduced to "+target.health)

        if(target.health <=0){
            target.faint();
        }
    }
    this.faint =  function(faint){
        console.log(this.name + " has fainted.");

    }
}

let pikachu = new Pokemon("Pikachu", 99);
console.log(pikachu);

let rattata = new Pokemon("Rattata", 5);
console.log(rattata);

pikachu.tackle(rattata);

rattata.tackle(pikachu);